package com.prashant.multithreading.producerconsumer.waitnotify;

import java.util.Queue;

/**
 * Created by prashant.sabnekar on 7/7/2017.
 */
public class Consumer implements Runnable {

    private Queue<Integer> queue;
    private String name;

    public Consumer(Queue<Integer> queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    public void run() {
        Thread.currentThread().setName(name);
        System.out.println("Consumer Thread started " + Thread.currentThread().getId() + " Name: " + Thread.currentThread().getName());
        while(true) {
            synchronized(queue) {
                while(queue.size() == 0) {
                    System.out.println("Queue Underflow. The queue is empty. The size of the queue is: " + queue.size());
                    try {
                        Thread.sleep(1000);
                        queue.wait();
                    } catch(InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                Integer element = queue.remove();
                System.out.println("Consumed element :::: " + element);
                queue.notifyAll();
            }
        }
    }
}
