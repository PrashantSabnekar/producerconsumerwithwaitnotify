package com.prashant.multithreading.producerconsumer.waitnotify;

import java.util.Queue;
import java.util.Random;

/**
 * Created by prashant.sabnekar on 7/7/2017.
 */
public class Producer implements Runnable {

    private Queue<Integer> queue;
    private String name;

    public Producer(Queue<Integer> queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    public void run() {
        Thread.currentThread().setName(name);
        System.out.println("Producer Thread started " + Thread.currentThread().getId() + " Name: " + Thread.currentThread().getName());
        while(true) {
            synchronized(queue) {
                while(queue.size() == 10) {
                    System.out.println("Queue is full. The Queue size is: " + queue.size());
                    try {
                        Thread.sleep(1500);
                        queue.wait();
                    } catch(InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                Random random = new Random();
                Integer number = random.nextInt();
                queue.add(number);
                System.out.println("Poduced element :::: " + number);
                queue.notifyAll();
            }
        }
    }
}
