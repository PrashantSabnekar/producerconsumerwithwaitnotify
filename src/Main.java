import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by prashant.sabnekar on 7/7/2017.
 */
public class Main {

    public static void main(String args[]) {

        Queue<Integer> queue = new LinkedList<>();
        Producer p1 = new Producer(queue, "Producer 1");
        Producer p2 = new Producer(queue, "Producer 2");
        Consumer c1 = new Consumer(queue, "Consumer 1");

        Thread pt1 = new Thread(p1);
        pt1.start();

        Thread pt2 = new Thread(p2);
        pt2.start();

        Thread pc1 = new Thread(c1);
        pc1.start();
    }
}
